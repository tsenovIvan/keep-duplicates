/*
I use a DSLR camera with 2 SD Cards.
Card one saves RAW and card two saves JPEG. The file names are the same, except the extension (NEF/JPEG)
I wrote this program, so I can delete unwanted files from the RAW folder and then, clean the JPEG folder automatically

Start this program in the Folder you want to CLEAN. Pass the already cleaned folder as argument number 1.
The end result will be local folder with only the file names present in the source folder (argument 1)
Mandatory 2nd parameter:

	-s will just show the files to be deleted
	-d will delete the files and print their names
*/
package main

import (
	"fmt"
	"os"
	"path"
	"sort"
)

var dirFileNames []string

func main() {
	if len(os.Args) == 1 {
		fmt.Println("Please enter a directory to compare!")
		return
	}

	if os.Args[1] == "-h" {
		fmt.Println("Please enter a directory to compare as 1st parameter.")
		fmt.Println("To immediately delete the missing files, pass -d as second parameter.")
		fmt.Println("To just show the missing files, pass -s as second parameter.")
	}

	if len(os.Args) < 3 {
		fmt.Println("Please pass second parameter, specifying the action!")
		fmt.Println("Pass -h to see the help.")
		return
	}

	fi, err := os.Stat(os.Args[1])

	if err != nil {
		fmt.Println(err)
		return
	}

	if fi.IsDir() {
		readDirFileNames(os.Args[1])
		cleanJpegDir()
	} else {
		fmt.Println("not Dir")
		return
	}

}

func readDirFileNames(dir string) {
	files, err := os.ReadDir(dir)

	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range files {
		if v.IsDir() {
			continue
		}
		ext := path.Ext(v.Name())
		name := v.Name()
		dirFileNames = append(dirFileNames, name[0:len(name)-len(ext)])
	}

	sort.Strings(dirFileNames)
}

func cleanJpegDir() {
	files, err := os.ReadDir(".")

	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range files {
		if v.IsDir() {
			continue
		}
		ext := path.Ext(v.Name())
		name := v.Name()
		nameOnly := name[0 : len(name)-len(ext)]

		if !containsName(dirFileNames, nameOnly) {
			if os.Args[2] == "-d" {
				err = os.Remove(name)
				if err != nil {
					fmt.Println(err)
					continue
				}
				fmt.Println("Removed file: " + name)
			}
			if os.Args[2] == "-s" {
				fmt.Println("Missing file: " + name)
			}
		}

	}
}

func containsName(slice []string, x string) bool {

	pos := sort.SearchStrings(slice, x)

	if pos != len(slice) && x == slice[pos] {
		return true
	}

	return false
}
