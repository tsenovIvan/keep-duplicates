# keep-duplicates
Keep duplicate files between 2 folders, deleting the missing ones

I use a DSLR camera with 2 SD Cards.
Card one saves RAW and card two saves JPEG. The file names are the same, except the extension (NEF/JPEG)

I wrote this program, so I can delete unwanted files from the RAW folder and then, clean the JPEG folder automatically

Start this program in the Folder you want to CLEAN. Pass the already cleaned folder as argument number 1.
The end result will be local folder with only the file names present in the source folder (argument 1)

Mandatory 2nd parameter:
-	-s will just show the files to be deleted
-	-d will delete the files and print their names
